# cryo-api

API to expose programs from cryo-stack

> At this moment there is no route to execute a set of instructions in multiple containers

## Scope

It creates an unified interface between the docker images in cryo-stack and the user. It can also be used to launch applications locally.

## The ServiceAPI

Calling this API will start a container from a given image, run a instruction, stop and remove the container.

It must be initiated with the name of the image and the directory that should be shared with the container.


## The API

The only route available is `exec` taking POST requests.

It takes one payload:

``` javascript
{
    image:        // the image the instructions should be sent,
    dir:          // what is the local directory that the container should have access to
    instructions: // an array of instructions that will be executed in the image
}
```

The server then takes the payload and parse it. Set a container up from the image requested, send the instructions in order, stop and remove the containers.

The data will be in the directory shared with the container.

### The API's reply

After executing the data, the API reply with a JSON object:

``` javascript
{
    error:   // If there is any error it will be here
    payload: // A copy of the payload sent to the call
    output:  // An array with the output of each instruction processed
}
```

## Best practices and tips

### Multiple calls vs. Multiple instructions.

Let's say we have to execute 10 commands in the imod package. There are 2 ways to do that: Multiple calls or Multiple instructions.

If we send multiple calls, each of the 10 calls will spawn a new container from the image, execute the instruction, stop the container and remove it.

If we send 1 call with multiple instruction, the API will spawn a container, run the 10 instructions, stop the container and remove it.

Obviously, the second is faster.

However, we may find a benefit from parallelizing some of these instructions if they are not dependent from each other.

We could send 5 instructions in 2 calls for example.

This flexibility might be handy to perform tasks more efficiently.

### Scripting multiple calls with bash

Another way to pass multiple calls and perhaps more complicated instructions, for example `for` loops and etc, is to use `bash` scripts.

The images come with bash out of the box and they can run bash scripts.

One way to perform a series of instructions is by making a bash script, placing it in the directory it will be shared with the container and pass the execution of the script as instruction to the call.

### Validation of pipeline config

All pipelines (classes extending the abstract Pipeline class) has to implement `validateConfig()` method publicaly.

This method is called by the API server before composing and executing the script.

It will throw an error if there are mistakes in the key values or if it is missing mandatory values.

## Pipelines

Pipelines are scripted in classes extending the abstract class `Pipeline.js`.

> Note: Don't forget to `.close()` instances of classes extending pipeline. It keeps the connection with Redis DB used by the Bull queue system. It may or maynot cause your script to hang. It does hang in tests.

## MovieMode pipeline

### Multiple work with 1 config

More often than not people put all their raw data into one directory because they have different prefix. It might be annoying to pass a single config file to add work to the queue. To facilitate this, and to keep it a clean API, we deal with this as follows.

When we initiate a new instance of `MovieMode` class, we must pass the base line configuration to it.

Then we must pass each `prefix` of the datasets in the directory to `.makeJob()`. `makeJob()` will return jobs that will later be added to the Bull Queue by the `cryoAPI` server.

### Dataset Unique Identifier - DUI

This is a 20 bit random number assigned to every run of the pipeline. Every run, even with exact same data and config will have different identifiers.

### Raw Data Unique Identifier - RDUI

The unique identifier of a dataset is calculated from the initial data and it is defined by first 20 characters of the sha-256 hash of the concatenated sha-256 hashes of each independent file.
