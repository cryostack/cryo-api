#!/bin/bash

echo "Creating persistent volume for Redis DB"
mkdir -p .redis/data;

echo "Checking if cryostack-redis is running already."

if [ $(docker ps -a -f "name=cryostack-redis" --format '{{.Names}}') ]
then
    echo "cryostack-redis is running, let's stop it."
    echo "Stoping container"
    docker stop cryostack-redis
    echo "Removing container"
    docker container rm cryostack-redis
    echo "Restarting container"
    docker run -d --name cryostack-redis --network host --publish 6379:6379 -v $PWD/.redis/data:/data redis:5.0.5-alpine
else
    echo "Starting cryostack-redis service"
    docker run -d --name cryostack-redis --network host --publish 6379:6379 -v $PWD/.redis/data:/data redis:5.0.5-alpine
fi