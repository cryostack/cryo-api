import { ServiceAPI } from '../src/ServiceAPI'

import * as chai from 'chai';
import chaiAsPromised from 'chai-as-promised'

chai.use(chaiAsPromised);
const expect = chai.expect;

describe('ServiceAPI', () => {
    describe('exec', () => {
        it('should provide error in answer if the image is not supported', async () => {
            const image = 'notSupported'
            const payload = {
                dir: '',
                instruction: '',
            }
            const service = new ServiceAPI(image, payload.dir)
            return expect(service.exec(payload.instruction)).to.be.rejected
        })
        it('should run command line applications', async function () {
            this.timeout(4000)
            const image = 'cryostack/imod:4.9.12'
            const payload = {
                dir: '',
                instruction: 'alignframes'
            }
            const expected = 0
            const service = new ServiceAPI(image, payload.dir)
            const answer = await service.exec(payload.instruction)
            expect(answer).eql(expected)
        })
        it('should run command line applications and produce data', async function () {
            this.timeout(4000)
            const image = 'cryostack/imod:4.9.12'
            const payload = {
                dir: process.env.PWD + '/testData',
                instruction: 'alignframes -in test_001.mrc -o AF_test001.mrc'
            }
            const expected = 0
            const service = new ServiceAPI(image, payload.dir)
            const answer = await service.exec(payload.instruction)
            expect(answer).eql(expected)
        })
    })
})
