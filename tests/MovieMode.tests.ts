import  { IMovieModeConfig, MovieMode } from '../src/pipelines/MovieMode'

import bunyan from 'bunyan'
import Bull from 'bull';
import * as chai from 'chai';
import chaiAsPromised from 'chai-as-promised'
import * as t from "io-ts";

chai.use(chaiAsPromised);
const expect = chai.expect;

describe('Pipeline::MovieMode', () => {
    const log = bunyan.createLogger({
        name: 'testPipeline::MovieMode'
    })
    describe('Integration', () => {
        it('should submit a valid MovieMode type job to the queue', async function () {
            this.timeout(120000)
            const queue = new Bull('cryostack-pipeline')
            await queue.empty()
            const validConfiguration: t.TypeOf<typeof IMovieModeConfig> = {
                "eTomoDirectives": {
                    "setupset.copyarg.dual": 1,
                },
                "cpus": 4,
                "path": "/home/d349cc25e/projects/repos/cryostack/dataset/movies",
                "tiltScheme": {
                    "increment": 2,
                    "mode": "bi-directional",
                    "range": [-60, 60],
                    "start": 0
                }
            }
            const mm = new MovieMode(validConfiguration, log)
            const job1 = await mm.makeJob('test')
            const processor = (job: Bull.Job) => mm.processor(job)
            queue.process(processor)
            const jobSent = await queue.add(job1)
            const state = await jobSent.finished()
                .then(() => jobSent.getState())
                .catch((err: Error) => {
                    throw err
                })
            await queue.close()
                .then(() => {
                    console.log('pipeline is closed')
                })
                .catch((err: Error) => {
                    throw err
                })
        
            expect(state).eql('completed')
        })
        it('should submit multiple MovieMode jobs with same config to the queue', async function () {
            this.timeout(120000)
            const queue = new Bull('cryostack-pipeline')
            await queue.empty()
            const validConfiguration: t.TypeOf<typeof IMovieModeConfig> = {
                "eTomoDirectives": {
                    "setupset.copyarg.dual": 1,
                },
                "cpus": 4,
                "path": "/home/d349cc25e/projects/repos/cryostack/dataset/movies",
                "tiltScheme": {
                    "increment": 2,
                    "mode": "bi-directional",
                    "range": [-60, 60],
                    "start": 0
                }
            }
            const mm = new MovieMode(validConfiguration, log)
            const job1 = await mm.makeJob('a.test')
            const job2 = await mm.makeJob('b.test')
            const processor = (job: Bull.Job) => mm.processor(job)
            queue.process(processor)
            const jobSent1 = await queue.add(job1)
            const jobSent2 = await queue.add(job2)
            const state1 = await jobSent1.finished()
                .then(() => jobSent1.getState())
                .catch((err: Error) => {
                    throw err
                })
            const state2 = await jobSent2.finished()
                .then(() => jobSent2.getState())
                .catch((err: Error) => {
                    throw err
                })
            await queue.close()
                .then(() => {
                    console.log('queue is closed')
                })
                .catch((err: Error) => {
                    throw err
                })
            expect([state1, state2]).eql(['completed', 'completed'])
        })
    })
})