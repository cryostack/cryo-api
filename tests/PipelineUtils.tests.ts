import { PipelineUtils } from '../src/PipelineUtils'

import * as t from "io-ts";
import * as chai from 'chai';
import { IConfig, ITiltScheme } from '../src/interfaces/pipeline.interfaces';

const expect = chai.expect;

describe('PipelineUtils', () => {
    describe('write', () => {
        it('should write empty script', async () => {
            const config: t.TypeOf<typeof IConfig> = {
                eTomoDirectives: {},
                cpus: 4,
                path: 'testData',
            }
            const pipeline = new PipelineUtils(config)
            pipeline.setPrefix('test')
            const script = pipeline.write()
            const expected = [
                '# This script has been generated automatically by cryoStack',
                '# It is CC0-v1.0, which means you can do whatever you want with it.',
                '# ... but... you probably don\'t want to mess around with it raw, like that.',
                '# instead, if I were you, I would use the cryoStack program to build one just for you.',
                '# Reach out for documentation at cryostack.org',
                '',
                '',
                '# End of script. No rights reserved what so ever.'
            ].join('\n')
            expect(script).eql(expected)
        })
    })
    describe('echoCurrentPrefix', () => {
        it('should take a file name as the pipeline input', () => {
            const config: t.TypeOf<typeof IConfig> = {
                eTomoDirectives: {},
                cpus: 4,
                path: 'testData',
            }
            const pipeline = new PipelineUtils(config)
            pipeline.setPrefix('test')
            const script = pipeline.echoCurrentPrefix()
                .write()
            const expected = [
                '# This script has been generated automatically by cryoStack',
                '# It is CC0-v1.0, which means you can do whatever you want with it.',
                '# ... but... you probably don\'t want to mess around with it raw, like that.',
                '# instead, if I were you, I would use the cryoStack program to build one just for you.',
                '# Reach out for documentation at cryostack.org',
                '',
                'echo test',
                '',
                '# End of script. No rights reserved what so ever.'
            ].join('\n')
            expect(script).eql(expected)
        })
    })
    describe('echoThis', () => {
        it('should write a line to print the argument string in the stdout.', () => {
            const config: t.TypeOf<typeof IConfig> = {
                eTomoDirectives: {},
                cpus: 4,
                path: 'testData',
            }
            const pipeline = new PipelineUtils(config)
            pipeline.setPrefix('test')
            const script = pipeline.echoThis('Hello cryo world!!!')
                .write()
            const expected = [
                '# This script has been generated automatically by cryoStack',
                '# It is CC0-v1.0, which means you can do whatever you want with it.',
                '# ... but... you probably don\'t want to mess around with it raw, like that.',
                '# instead, if I were you, I would use the cryoStack program to build one just for you.',
                '# Reach out for documentation at cryostack.org',
                '',
                'echo \'Hello cryo world!!!\'',
                '',
                '# End of script. No rights reserved what so ever.'
            ].join('\n')
            expect(script).eql(expected)
        })
    })
    describe('alignframes', () => {
        it('should write instructions to alignframes', () => {
            const config: t.TypeOf<typeof IConfig> = {
                eTomoDirectives: {},
                cpus: 4,
                path: 'testData',
            }
            const pipeline = new PipelineUtils(config)
            pipeline.setPrefix('test')
            const script = pipeline.alignFrames()
                .write()
            const expected = [
                '# This script has been generated automatically by cryoStack',
                '# It is CC0-v1.0, which means you can do whatever you want with it.',
                '# ... but... you probably don\'t want to mess around with it raw, like that.',
                '# instead, if I were you, I would use the cryoStack program to build one just for you.',
                '# Reach out for documentation at cryostack.org',
                '',
                'for i in `ls test*.mrc`',
                '  do alignframes -in ${i} -o af.${i} ',
                'done',
                '',
                '# End of script. No rights reserved what so ever.'
            ].join('\n')
            expect(script).eql(expected)
        })
    })
    describe('stackFranes', () => {
        it('should write the lines to stack frames correctly', () => {
            const config: t.TypeOf<typeof IConfig> = {
                eTomoDirectives: {},
                cpus: 4,
                path: 'testData'
            }
            const tiltScheme: t.TypeOf<typeof ITiltScheme> = {
                increment: 2,
                mode: 'bi-directional',
                range: [-60, 60],
                start: 0
            }
            const pipeline = new PipelineUtils(config)
            pipeline.setPrefix('test')
            const script = pipeline.stackFrames(tiltScheme)
                .write()
            const expected = [
                '# This script has been generated automatically by cryoStack',
                '# It is CC0-v1.0, which means you can do whatever you want with it.',
                '# ... but... you probably don\'t want to mess around with it raw, like that.',
                '# instead, if I were you, I would use the cryoStack program to build one just for you.',
                '# Reach out for documentation at cryostack.org',
                '',
                '# sanity check',
                'n=`ls test*.mrc | wc -l`',
                'if [ $n -ne 61 ]',
                '  then echo "We can\'t process this tilt series, it seems that it is incomplete. Please execute the reconstruction manually or check your files."; exit 1',
                'fi',
                'newstack `ls -r test*.mrc | head -n 30` first.mrc',
                'newstack `ls test*.mrc | head -n 31` second.mrc',
                'newstack first.mrc second.mrc stacked.test.mrc',
                'cp stacked.test.mrc ts.stacked.test.mrc',
                '',
                '# End of script. No rights reserved what so ever.'
            ].join('\n')
            expect(script).eql(expected)
        })
        it('should take the parameters and write the correct lines to stack frames correctly starting -20', () => {
            const config: t.TypeOf<typeof IConfig> = {
                eTomoDirectives: {},
                cpus: 4,
                path: 'testData'
            }
            const tiltScheme: t.TypeOf<typeof ITiltScheme> = {
                increment: 2,
                mode: 'bi-directional',
                range: [-60, 60],
                start: -20
            }
            const pipeline = new PipelineUtils(config)
            pipeline.setPrefix('test')
            const script = pipeline.stackFrames(tiltScheme)
                .write()
            const expected = [
                '# This script has been generated automatically by cryoStack',
                '# It is CC0-v1.0, which means you can do whatever you want with it.',
                '# ... but... you probably don\'t want to mess around with it raw, like that.',
                '# instead, if I were you, I would use the cryoStack program to build one just for you.',
                '# Reach out for documentation at cryostack.org',
                '',
                '# sanity check',
                'n=`ls test*.mrc | wc -l`',
                'if [ $n -ne 61 ]',
                '  then echo "We can\'t process this tilt series, it seems that it is incomplete. Please execute the reconstruction manually or check your files."; exit 1',
                'fi',
                'newstack `ls -r test*.mrc | head -n 20` first.mrc',
                'newstack `ls test*.mrc | head -n 41` second.mrc',
                'newstack first.mrc second.mrc stacked.test.mrc',
                'cp stacked.test.mrc ts.stacked.test.mrc',
                '',
                '# End of script. No rights reserved what so ever.'
            ].join('\n')
            expect(script).eql(expected)
        })
        it('should take the parameters and write the correct lines to stack frames correctly starting in -10', () => {
            const config: t.TypeOf<typeof IConfig> = {
                eTomoDirectives: {},
                cpus: 4,
                path: 'testData',
            }
            const tiltScheme: t.TypeOf<typeof ITiltScheme> = {
                increment: 2,
                mode: 'bi-directional',
                range: [-60, 60],
                start: -10
            }
            const pipeline = new PipelineUtils(config)
            pipeline.setPrefix('test')
            const script = pipeline.stackFrames(tiltScheme)
                .write()
            const expected = [
                '# This script has been generated automatically by cryoStack',
                '# It is CC0-v1.0, which means you can do whatever you want with it.',
                '# ... but... you probably don\'t want to mess around with it raw, like that.',
                '# instead, if I were you, I would use the cryoStack program to build one just for you.',
                '# Reach out for documentation at cryostack.org',
                '',
                '# sanity check',
                'n=`ls test*.mrc | wc -l`',
                'if [ $n -ne 61 ]',
                '  then echo "We can\'t process this tilt series, it seems that it is incomplete. Please execute the reconstruction manually or check your files."; exit 1',
                'fi',
                'newstack `ls -r test*.mrc | head -n 25` first.mrc',
                'newstack `ls test*.mrc | head -n 36` second.mrc',
                'newstack first.mrc second.mrc stacked.test.mrc',
                'cp stacked.test.mrc ts.stacked.test.mrc',
                '',
                '# End of script. No rights reserved what so ever.'
            ].join('\n')
            expect(script).eql(expected)
        })
        it('should write the correct lines to stack frames correctly starting in -21, increment of 3 and range -60 to 60', () => {
            const config: t.TypeOf<typeof IConfig> = {
                eTomoDirectives: {},
                cpus: 4,
                path: 'testData',
            }
            const tiltScheme: t.TypeOf<typeof ITiltScheme> = {
                increment: 3,
                mode: 'bi-directional',
                range: [-60, 60],
                start: -21
            }
            const pipeline = new PipelineUtils(config)
            pipeline.setPrefix('test')
            const script = pipeline.stackFrames(tiltScheme)
                .write()
            const expected = [
                '# This script has been generated automatically by cryoStack',
                '# It is CC0-v1.0, which means you can do whatever you want with it.',
                '# ... but... you probably don\'t want to mess around with it raw, like that.',
                '# instead, if I were you, I would use the cryoStack program to build one just for you.',
                '# Reach out for documentation at cryostack.org',
                '',
                '# sanity check',
                'n=`ls test*.mrc | wc -l`',
                'if [ $n -ne 41 ]',
                '  then echo "We can\'t process this tilt series, it seems that it is incomplete. Please execute the reconstruction manually or check your files."; exit 1',
                'fi',
                'newstack `ls -r test*.mrc | head -n 13` first.mrc',
                'newstack `ls test*.mrc | head -n 28` second.mrc',
                'newstack first.mrc second.mrc stacked.test.mrc',
                `cp stacked.test.mrc ts.stacked.test.mrc`,
                '',
                '# End of script. No rights reserved what so ever.'
            ].join('\n')
            expect(script).eql(expected)
        })
    })
    describe('buildBatchruntomoDirectiveFile', () => {
        it('should return a string with the content of a directive file', () => {
            const config: t.TypeOf<typeof IConfig> = {
                eTomoDirectives: {},
                cpus: 4,
                path: 'testData',
            }
            const pipeline = new PipelineUtils(config)
            const expected = [
                '# This file was created automatically by cryoStack',
                '# As with anything in this package, you can do whatever you want with it.',
                '# But... it will make your life easier in the future if you change the config file fed to the cryoStack pipeline.',
                '# Check it out the documentation at cryostack.org',
                '# Cheers',
                '',
                'setupset.copyarg.dual = 1',
                'setupset.copyarg.montage = 0',
                'setupset.copyarg.firstinc = 60.0, -2.0',
                'setupset.scanHeader = 0',
                'setupset.copyarg.extract = 0',
                'setupset.copyarg.rotation = 84.7',
                'setupset.copyarg.pixel = 0.5442',
                'setupset.copyarg.gold = 10',
                'runtime.AlignedStack.any.correctCTF = 0',
                'comparam.xcorr.tiltxcorr.SearchMagChanges = 0',
                'comparam.xcorr.tiltxcorr.FilterRadius2 = 0.2',
                'comparam.xcorr.tiltxcorr.FilterSigma2 = 0.01',
                'runtime.Fiducials.any.fiducialless = 1',
                'comparam.prenewst.newstack.BinByFactor = 3',
                'runtime.AlignedStack.any.binByFactor = 3',
                'runtime.Preprocessing.any.removeXrays = 0',
                'runtime.Fiducials.any.trackingMethod = 0',
                'runtime.Fiducials.any.seedingMethod = 1',
                'comparam.track.beadtrack.SobelFilterCentering = 1',
                'comparam.track.beadtrack.KernelSigmaForSobel = 1',
                'comparam.track.beadtrack.RoundsOfTracking = 4',
                'runtime.BeadTracking.any.numberOfRuns = 4',
                'comparam.autofidseed.autofidseed.TargetNumberOfBeads = 25',
                'comparam.autofidseed.autofidseed.AdjustSizes = 0',
                'comparam.autofidseed.autofidseed.TwoSurfaces = 0',
                'comparam.align.tiltalign.SurfacesToAnalyze = 2',
                'comparam.align.tiltalign.LocalAlignments = 1',
                'comparam.align.tiltalign.RobustFitting = 0',
                'runtime.TiltAlignment.any.enableStretching = 1',
                'comparam.tilt.tilt.THICKNESS = 1000',
                'comparam.tilt.tilt.FakeSIRTiterations = 5',
                'runtime.Trimvol.any.reorient = 2',
            ].join('\n')
            const directiveString = pipeline.buildBatchruntomoDirectiveFile()
            expect(directiveString).eql(expected)
        })
    })
    describe('MakeId', () => {
        it('should make ID from the raw dataset', async () => {
            const config: t.TypeOf<typeof IConfig> = {
                eTomoDirectives: {},
                cpus: 4,
                path: 'testData',
            }
            const pipeline = new PipelineUtils(config)
            pipeline.setPrefix('test')
            const id = await pipeline.makeId()
            const expected = 'da01655f1eae5aaa53e1'
            expect(id).eql(expected)
        })
    })
})

