import { DockerControl } from '../src/DockerControl'

import * as chai from 'chai';
import chaiAsPromised from 'chai-as-promised'
import bunyan from 'bunyan'
import { exec } from 'child-process-promise';

chai.use(chaiAsPromised);
const expect = chai.expect;



describe('DockerControl', () => {
    let log = bunyan.createLogger({
        name: 'test'
    })
    describe('start', () => {
        it('should start the container given an image from cryostack', async () => {
            const dc = new DockerControl({log})
            const options = {
                dir: 'testData'
            }
            const result = await dc.start('cryostack/imod:4.9.12', options)
            // tslint:disable-next-line: no-unused-expression
            expect(result.id).be.string
        })
        it('should not start the container from inexistent images', function () {
            this.timeout(60000)
            const dc = new DockerControl({log})
            const options = {
                dir: 'testData'
            }
            return expect(dc.start('badimage', options)).to.be.rejected
        })
    })
    describe('list', () => {
        it('should provide a list of containers managed by the instance', async () => {
            const dc = new DockerControl({log})
            const options = {
                dir: 'testData'
            }
            const result = await dc.start('cryostack/imod:4.9.12', options)
            const list = dc.list()
            expect(list.length).eql(1)
        })
        it('should provide a list of containers managed by the instance', async () => {
            const dc = new DockerControl({log})
            const options = {
                dir: 'testData'
            }
            const result1 = await dc.start('cryostack/imod:4.9.12', options)
            const result2 = await dc.start('cryostack/imod:4.9.12', options)
            const list = dc.list()
            expect(list.length).eql(2)
        })
    })
    describe('stop', () => {
        it('should stop', async () => {
            const dc = new DockerControl({log})
            const options = {
                dir: 'testData'
            }
            const result = await dc.start('cryostack/imod:4.9.12', options)
            const id = result.id
            await dc.stop(id)
            const list = dc.list()
            expect(list.length).eql(0)
            const data = await exec('docker ps -a -q')
            const containers = data.stdout.split('\n')
            containers.pop()
            const findMatch = containers.filter((containerId: string) => {
                return id.match(containerId)
            })
            // tslint:disable-next-line: no-unused-expression
            expect(findMatch).to.be.empty
        })
        it('should be rejected if try to stop container not managed by instance', async () => {
            const dc = new DockerControl({log})
            const options = {
                dir: 'testData'
            }
            const result = await dc.start('cryostack/imod:4.9.12', options)
            const id = result.id + 'notAnID'
            return expect(dc.stop(id)).to.be.rejected
        })
    })
    describe('exec', () => {
        it('should throw and stop execution if there is an Error in the container.', async () => {
            const dc = new DockerControl({log})
            const options = {
                dir: 'testData'
            }
            const container = await dc.start('cryostack/imod:4.9.12', options)
            const badInstruction = 'ws'
            const result = dc.exec(badInstruction, container.id)
            const rejectionExpected = '137'
            expect(result).to.eventually.be.rejectedWith(rejectionExpected)
        })
    })
    after(async function () {
        this.timeout(5000)
        try {
            await exec('docker stop $(docker ps -a -f ancestor=cryostack/imod:4.9.12 -q)')
        }
        catch(err) {}
    })
})