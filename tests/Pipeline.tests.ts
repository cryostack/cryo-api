import { Pipeline } from '../src/Pipeline'

import bunyan from 'bunyan'
import * as chai from 'chai';
import * as t from "io-ts";

import { IJob } from '../src/interfaces/pipeline.interfaces';
import Bull from 'bull';

const expect = chai.expect;
const log = bunyan.createLogger({
    name: 'Test'
})

class MockPipeline extends Pipeline {

    constructor() {
        super('test', log)
    }

    public validateConfig() {
        return true
    }

    public makeJob(job: string) {
        return job
    }

}

describe('Pipeline', () => {
    describe('getName()', () => {
        it('should return its name', () => {
            const job1 = {
                dir: 'testData',
                dataId: '1thisjobbmakessleep5',
                image: 'cryostack/imod:4.9.12',
                instruction: `echo 'I am job 1'; sleep 1`
            }
           
            const mock = new MockPipeline()
            const name = mock.getName()
            expect(name).eql('test')
        })
    })
    describe('Integration', function () {
        it('should make a sucessful job', async function () {
            this.timeout(8000)
            const queue = new Bull('cryostack-pipeline')
            await queue.empty()
            const job1 = {
                dir: 'testData',
                dataId: '1thisjobbmakessleep5',
                image: 'cryostack/imod:4.9.12',
                instruction: `echo 'I am job 1'; sleep 1`
            }
            const mock = new MockPipeline()
            const processor = ((job: Bull.Job) => mock.processor(job))
            queue.process(processor)
            const jobSent = await queue.add(job1)
            const state = await jobSent.finished()
                .then(() => jobSent.getState())
                .catch((err: Error) => {
                    throw err
                })
            await queue.close()
                .then(() => {
                    console.log('queue is closed')
                })
                .catch((err: Error) => {
                    throw err
                })
            expect(state).eql('completed')
        })
    })
})
