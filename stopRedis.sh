#!/bin/bash

echo "Creating persistent volume for Redis DB"
mkdir -p .redis/data;

echo "Checking if cryostack-redis is running."

if [ $(docker ps -a -f "name=cryostack-redis" --format '{{.Names}}') ]
then
    echo "cryostack-redis is running, let's stop it."
    echo "Stoping container"
    docker stop cryostack-redis
    echo "Removing container"
    docker container rm cryostack-redis
fi
