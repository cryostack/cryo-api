'use strict'

import bunyan from 'bunyan'
import fs from 'fs'
import * as t from "io-ts";
import { reporter } from 'io-ts-reporters'

import { IConfig, IJob, ITiltScheme } from '../interfaces/pipeline.interfaces';
import { Pipeline } from '../Pipeline';
import { PipelineUtils } from '../PipelineUtils';
import Bull from 'bull';

const defaults = {
    batchruntomoDirectiveFilePrefix: (id: string, n: number):string => `cryoStack.MovieMode.etomoDirectiveFile.${id}.${n}`,
    scriptFilename: (id: string, n: number):string => `cryoStack.pipelineScript.${id}.${n}.sh`,
    sizeOfIdSlice: 20,
    image: 'cryostack/imod:4.9.12'
}

const optionalTilt = t.partial({
    tiltScheme: ITiltScheme
})

const IMovieModeConfig = t.intersection([
    IConfig,
    optionalTilt
])

class MovieMode extends Pipeline {
    private config: t.TypeOf<typeof IMovieModeConfig>
    private now: number
    private pUtils: PipelineUtils

    constructor(config: t.TypeOf<typeof IMovieModeConfig>, log: bunyan) {
        const image = 'imod'
        const name = 'MovieModeConfig'
        super(name, log)
        this.config = config
        this.now = Date.now()
        this.pUtils = new PipelineUtils(this.config)
    }

    public async processor(job: Bull.Job) {
        job.log(job.data)
        return await super.processor(job)
    }

    public async makeJob(prefix: string) {
        this.now = Date.now()
        this.pUtils.resetScript()
        if (this.validateConfig()) {
            this.pUtils.setPrefix(prefix)
            const id = await this.pUtils.makeId()
            const scriptName = defaults.scriptFilename(id.slice(12), this.now)
            this.compose(id)
            super.loadPath(this.config.path).saveScript(scriptName)
            const job: t.TypeOf<typeof IJob> = {
                dataId: id,
                dir: this.config.path,
                image: defaults.image,
                instruction: `sh ${scriptName}`
            }
            return job
        }
    }

    public validateConfig(): boolean {
        const result = IMovieModeConfig.decode(this.config)
        const reports = reporter(result)
        const errors = []
        for (const report of reports) {
            this.log.error(report)
            errors.push(report)
        }
        if (errors.length !== 0) {
            throw new Error(errors.join('\n'))
        }
        return true
    }

    private compose(id: string) {
        this.log.info(`Staging files to be used in ${super.getName()} reconstruction pipeline with files.`)
        const directivesConfig = this.pUtils.buildBatchruntomoDirectiveFile()
        const filename = `${defaults.batchruntomoDirectiveFilePrefix(id, this.now)}.adoc`
        const filenamePath = this.config.path + '/' + filename
        fs.writeFileSync(filenamePath, directivesConfig)
        let script = this.pUtils.echoThis('Working on: ')
                .echoCurrentPrefix()
                .alignFrames()
        if (this.config.tiltScheme) {
            script = script.stackFrames(this.config.tiltScheme)
        }
        
        this.script = script.batchruntomo(filename).write()
        return this
    }

}

export {
    MovieMode,
    IMovieModeConfig
}