import bunyan from 'bunyan'
import { Stream } from 'stream';
import { DockerControl } from './DockerControl'

class ServiceAPI {
    private containerId: string
    private image: string
    private dir: string
    private log: bunyan;
    private dockerControl: DockerControl;
    private output: number
    // private dockerLog: import("stream").Transform;

    constructor(image: string, dir: string) {
        this.containerId = ''
        this.image = image
        this.dir = dir
        this.log = bunyan.createLogger({
            name: `CryoStack::ServiceAPI::${image}`
        })
        this.dockerControl = new DockerControl({log: this.log})
        this.output = -1
    }

    public logStream() {
        const dockerLog = this.dockerControl.logStream()
        const aStream = new Stream.Transform()
        aStream._write = function (chunk, enc, next) {
            this.push(chunk);
            next()
        }
        this.log.addStream({
            stream: aStream
        })
        dockerLog.on('data', (data) => {
            aStream.push(data)
        })
        return aStream
    }

    public exec = async (instruction: string) => {
        await this.startContainers()
            .then(() => this.run(instruction))
            .then(this.stopContainers)
        return this.output
    }

    private startContainers = async () => {
        const dockerOptions = {
            dir: this.dir
        }
        const containerInfo = await this.dockerControl.start(this.image, dockerOptions)
        this.log.info('Container info:')
        this.log.info(containerInfo)
        this.containerId = containerInfo.id
        return this
    }

    private stopContainers = async () => {
        await this.dockerControl.stopAll()
    }

    private run = async (instruction: string) => {
        const result = await this.dockerControl.exec(instruction, this.containerId)
        this.log.info(`Instruction "${instruction}" has been executed.`)
        this.output = parseInt(result)
        return this
    }
}

export {
    ServiceAPI
}