import  { MovieMode } from './pipelines/MovieMode'

class DynamicPipelines {
    public start(pipeline:string) {
        switch(pipeline) {
            case 'imod/movie-mode':
                return MovieMode
            default:
                throw new Error('This pipeline is not available')
        }
    }
}

export {
    DynamicPipelines
}