#!/usr/bin/env node

import chalk from 'chalk'
import figlet from 'figlet'
import path from 'path'
import shell from 'shelljs'

chalk.enabled = true;

// tslint:disable-next-line: no-var-requires
const pkjson = require('../../package.json');

const positionOfArg = 2
const args = process.argv.slice(positionOfArg).join(' ')
const script = path.resolve(__dirname, '../cli/cryostack-cli.js')

const splash = figlet.textSync('CryoStack', {horizontalLayout: 'fitted'})
console.log(chalk.cyan(splash))
console.log(`\t\t\t\t\t\t\t            ${chalk.cyan('' + pkjson.version)}`)
console.log(chalk.red('\t\t\t\t\t\t\t\t\t\t\t   by Davi Ortega'))
shell.exec('node ' + script + ' ' + args)
