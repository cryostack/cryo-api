'use strict'

import bunyan from 'bunyan'
import fs from 'fs'
import * as t from "io-ts";

import Bull from 'bull'
import { Stream } from 'stream';
import { IJob } from './interfaces/pipeline.interfaces';
import { ServiceAPI } from './ServiceAPI';

abstract class Pipeline {
    protected log: bunyan
    protected script: string

    private name: string
    private path: string

    constructor(name: string, log: bunyan) {
        this.name = name
        this.path = ''
        this.script = ''
        this.log = log.child({
            'library': `CryoStack::Pipeline::${this.name}`
        })
    }

    public loadPath(path: string) {
        this.path = path
        return this
    }

    public getName() {
        this.log.info('Returning job name')
        return this.name
    }

    public logStream() {
        const aStream = new Stream.Transform()
        aStream._write = function (chunk, enc, next) {
            this.push(chunk);
            next()
        }
        this.log.addStream({
            stream: aStream
        })
        return aStream
    }

    public showScript() {
        return this.script
    }

    public saveScript(scriptFilename: string) {
        this.log.info(`Saving script in working directory`)
        fs.writeFileSync(this.path + '/' + scriptFilename, this.script)
        return this
    }

    public async processor(job: Bull.Job) {
        return await this.defaultProcessor(job)
    }

    private async defaultProcessor(job: Bull.Job) {
        const nextJob = job.data as t.TypeOf<typeof IJob>
        const service = new ServiceAPI(nextJob.image, nextJob.dir)
        service.logStream().on('data', (data) => {
            job.log(data)
        })
        return service.exec(nextJob.instruction)
            .then(async (output) => {
                if (output === 0) {
                    job.log(`The job ${nextJob.dataId} is complete`)
                }
                else {
                    job.log(`The job ${nextJob.dataId} exited successfully, but with a non-zero code: ${output}.`)
                }
            })
            .catch(async (err) => {
                job.log(`The job ${nextJob.dataId} failed`)
                job.log(`This is the error log: `)
                job.log(err)
                throw err
            })
    }

    protected abstract validateConfig(): void
    protected abstract makeJob(prefix: string): void
}

export {
    Pipeline,
}
