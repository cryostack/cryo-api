import bodyParser = require('body-parser');
import bunyan from 'bunyan'
import cors from 'cors';
import express from 'express';

import Bull from 'bull'
import { DynamicPipelines } from './DynamicPipelines'

import { imageAliases } from './config/cryostack.config'
import { ServiceAPI } from './ServiceAPI';
const imagesAvailable = Object.values(imageAliases)


const log = bunyan.createLogger({
  name: 'CryoStack::API'
})

const queue = new Bull('cryostack-pipeline')

// Create a new express application instance
const app: express.Application = express();

const corsOptions = {
  allowHeaders: ['Content-Type'],
  methods: ['POST'],
  optionsSuccessStatus: 200, // some legacy browsers (IE11, various SmartTVs) choke on 204
  origin: 'http://localhost',
}

app.options('/services/:alias', cors())
app.options('/pipelines/', cors())


app.use(bodyParser.json()) // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true })) // for parsing application/x-www-form-urlencoded

app.get('/log/:id', (req, res) => {
  res.send()
})

// tslint:disable-next-line: variable-name
app.get('/status', (_req, res) => {
  res.send('API is listening');
});

app.post('/services/:alias', cors(), async (req, res, next) => {
    log.info('Instruction received to execute a command from an image')
    const payload = req.body
    log.info(JSON.stringify(req.body))
    let image = req.params.alias
    if (req.body.image) {
      const specificImage = req.body.image
      if (imagesAvailable.indexOf(specificImage) !== -1) {
        log.info(`Found image "${req.body.image}" passed in payload. Overwriting route`)
        image = specificImage
      }
      else {
        log.warn(`Image found in payload does not exit. Will try to use image alias passed in route`)
      }
    }
    const aliases = imageAliases.filter((items) => items.alias === req.params.alias)
    if (aliases) {
      image = aliases[0].image
      log.info(`Found "${req.params.alias}" as alias for "${image}" in config file.`)
    } 
    else {
      log.warn(`Did not find alias "${image}", trying to run it with this image.`)
    }

    const service = new ServiceAPI(image, payload.dir) 
    const logPipeline = service.logStream()
    logPipeline.on('data', (data) => {
      res.write(data)
    })
    await service.exec(payload.instruction).then(() => {
        log.info('The call has been answered')
        res.end()
    })
    .catch((err) => {
      log.error(err)
      next(err)
      // res.status(500).send({error: err})
    })
})

app.post('/pipelines', cors(), async (req, res, next) => {
  log.info('Instructions received to start a pipeline')
  const payLoad = req.body
  if (payLoad.pipeline && payLoad.config && payLoad.prefixList) {
    const pipelineChoice = String(payLoad.pipeline)
    const config = payLoad.config
    const prefixList = payLoad.prefixList
    const dyPipe = new DynamicPipelines()
    const Pipeline = dyPipe.start(pipelineChoice)
    const p = new Pipeline(config, log)
    try {
      queue.process(p.getName(),(job: Bull.Job) => p.processor(job))
    }
    catch {
      log.warn(`Trying to set same processor again. Can't do... skipping`)
    }
    const submitJobs = async () => {
      for (let prefix of prefixList) {
        await p.makeJob(prefix)
          .then((job) => queue.add(p.getName(), job))
          .catch((err) => {
            throw err
          })
      }
    }

    await submitJobs()
      .then(() => {
        res.end()
        next()
      })
      .catch((err) => {
        log.error(err)
        console.log(err)
        next(err)
        return
      })
  }
  else {
    log.error(`Missing fields`)
    next(`Missing fields`)
  }
})


app.listen(3001, () => {
  log.info('Example app listening on port 3001!');
});

