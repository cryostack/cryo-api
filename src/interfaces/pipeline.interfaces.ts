// types.ts 
import * as t from "io-ts";

import { IDirectives } from "./batchruntomo.interfaces";

const ITiltScheme = t.interface({
    increment: t.number,
    mode: t.keyof({
        'bi-directional': null,
        'directional': null,
        'dose-symmetric': null,
        'group-dose-symmetric': null
    }),
    range: t.tuple([t.number, t.number]),
    start: t.number
})

const IConfig = t.interface({
    cpus: t.number,
    eTomoDirectives: IDirectives,
    path: t.string,
})

const IWork = t.interface({
    config: IConfig,
    prefixList: t.string,
})

const IJob = t.interface({
    dir: t.string,
    dataId: t.string,
    image: t.string,
    instruction: t.string,
})

export {
    IConfig,
    ITiltScheme,
    IJob
}