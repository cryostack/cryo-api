import bunyan from 'bunyan'
import { exec } from 'child_process';
import { Stream } from 'stream';

interface IDockerControlOptions {
    dir: string
}

interface IContainer {
    image: string,
    id: string
}

class DockerControl {
    private containers: IContainer[]
    private log: bunyan
    
    constructor(options: {log: bunyan}) {
        this.containers = []
        this.log = options.log.child({
            'library': 'DockerControl'
        })
    }

    public logStream() {
        const aStream = new Stream.Transform()
        aStream._write = function (chunk, enc, next) {
            this.push(chunk);
            next()
        }
        this.log.addStream({
            stream: aStream
        })
        return aStream
    }

    public list(): IContainer[] {
        return this.containers
    }

    public async start(image: string, options: IDockerControlOptions): Promise<IContainer> {
        return new Promise((resolve, reject) => {
            const command = `docker run -ti -d --rm -u cryostack -v ${options.dir}:/home/cryostack/workdir -v /tmp/.X11-unix:/tmp/.X11-unix -e DISPLAY ${image} /bin/bash`
            this.log.info(`Running full command: ${command}`)
            const child = exec(command);
            const container = {
                id: 'unknown',
                image
            };
            if (child.stdout) {
                child.stdout.on('data', (output) => {
                    this.log.info(`start() log: ${output}`)
                    container.id = output.replace('\n', '')
                })
            }
            if (child.stderr) {
                child.stderr.on('data', (data) => {
                    const rawLogEntries = data.toString().split('\n')
                    for (const rawLogEntry of rawLogEntries) {
                        if (rawLogEntry) {
                            this.log.error(rawLogEntry)
                        }
                    }
                })
            }
            child.on('close', (output: string) => {
                if (parseInt(output, 10) !== 0) {
                    this.log.warn(`Docker run command exited with status ${output}`)
                    reject(output)
                    return
                }
                this.log.info('Container successfully started')
                this.log.info(container)
                this.containers.push(container)
                resolve(container)
            })
            child.on('error', (err) => {
                this.log.error('Container did not start')
                this.log.error(err)
                reject(err)
            })
        })
    }

    public async exec(command: string, containerId: string): Promise<string> {
        return new Promise((resolve, reject) => {
            this.log.info(`Passing command ${command} to container ${containerId}`)
            const dockerCommand = `docker exec -u cryostack ${containerId} /bin/bash -c "${command}"`
            this.log.info(`Running full command: ${dockerCommand}`)
            const child = exec(dockerCommand)

            if (child.stdout) {
                child.stdout.on('data', (data) => {
                    const rawLogEntries = data.toString().split('\n')
                    let error = false
                    for (const rawLogEntry of rawLogEntries) {
                        if (rawLogEntry) {
                            if (rawLogEntry.match(/ERROR/)) {
                                this.log.error(`This was not passed as a fatal Error, but it seems like one:`)
                                error = true
                                break
                            }
                            else {
                                this.log.info(`Logs from container ${containerId}: ${rawLogEntry}`)
                            }
                        }
                    }
                    if (error) {
                        for (const rawLogEntry of rawLogEntries) {
                            this.log.error(rawLogEntry)
                        }
                        reject(1)
                    }
                })
            }
            if (child.stderr) {
                child.stderr.on('data', async (data) => {
                    this.log.error(`Listener got some bad news from docker`)
                    const rawLogEntries = data.toString().split('\n')
                    let error = true
                    for (const rawLogEntry of rawLogEntries) {
                        if (rawLogEntry) {
                            if (rawLogEntry.match(/^libGL/)) {
                                this.log.warn(`known error from libGL not being in the imod image. skipping`)
                                error = false
                                break
                            }
                            this.log.error(rawLogEntry)
                        }
                    }
                    if (error) {
                        this.log.error(`Let's stop this process as soon as possible. It might take a bit, but it is as fast as we can.`)
                        await this.stop(containerId)
                        throw rawLogEntries
                    }
                })
            }
            child.on('close', (output: string) => {
                if (parseInt(output, 10) === 137) {
                    this.log.error(`Execution interrupted.`)
                    this.log.error(`Docker exec command exited with status ${output}`)
                    this.log.error(`This is likely caused by something earlier. Use "find" tool and search for errors in the log prior to this.`)
                    reject(output)
                }
                else if (parseInt(output, 10) === 127) {
                    this.log.error(`Docker exec command exited with status ${output}`)
                    reject(output)
                }
                else if (parseInt(output, 10) !== 0) {
                    this.log.warn(`Docker exec command exited with status ${output}`)
                    reject(output)
                }
                else {
                    this.log.info('Execution completed')
                    resolve(output)   
                }
            })
            child.on('error', (err) => {
                this.log.error('Execution interrupted')
                this.log.error(err)
                reject(err)
            })
        })
    }

    public stop(containerId: string) {
        return new Promise((resolve, reject) => {
            this.log.info(`From the containers below we must stop ${containerId}`)
            this.log.info(this.containers)
            const containerIds = this.containers.map((container) => container.id)
            console.log(`Container Ids: ${containerIds}`)
            this.log.info(`Container Ids: ${containerIds}`)
            if (containerIds.indexOf(containerId) !== -1) {
                this.log.info(`Found container that needs to be stopped`)
                const newList = this.containers.filter((container) => {
                    return container.id !== containerId
                })

                this.containers = newList
                const data = exec(`docker container stop ${containerId}`)
                data.on('close', (output: string) => {
                    this.log.info(`Container stopped with success`)
                    resolve(output)
                })
                data.on('error', (error) => {
                    this.log.error(`Container could not be stopped`)
                    reject(error)
                })
            }
            else {
                this.log.error(`The container ${containerId} is not managed by this DockerControl instance. We will do nothing.`)
                reject(`The container ${containerId} is not managed by this DockerControl instance. We will do nothing.`)
            }
        })
    }

    public async stopAll() {
        const containerIds = this.containers.map((container) => container.id)
        for (const cId of containerIds) {
            await this.stop(cId)
        }
        return
    }


}

export {
    DockerControl
}