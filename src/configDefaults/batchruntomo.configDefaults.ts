// tslint:disable: object-literal-sort-keys
import * as t from "io-ts";
import { IDirectives } from '../interfaces/batchruntomo.interfaces'

const defaults: t.TypeOf<typeof IDirectives> = {
    'setupset.copyarg.dual': 0,
    'setupset.copyarg.montage': 0,
    'setupset.copyarg.firstinc': '60.0, -2.0',
    'setupset.scanHeader': 0,
    'setupset.copyarg.extract': 0,
    'setupset.copyarg.rotation': 84.7,
    'setupset.copyarg.pixel': 0.5442,
    'setupset.copyarg.gold': 10,
    'runtime.AlignedStack.any.correctCTF': 0,
    'comparam.xcorr.tiltxcorr.SearchMagChanges': 0,
    'comparam.xcorr.tiltxcorr.FilterRadius2': 0.2,
    'comparam.xcorr.tiltxcorr.FilterSigma2': 0.01,
    'runtime.Fiducials.any.fiducialless': 1, // Really?
    'comparam.prenewst.newstack.BinByFactor': 3,
    'runtime.AlignedStack.any.binByFactor': 3,
    'runtime.Preprocessing.any.removeXrays': 0,
    'runtime.Fiducials.any.trackingMethod': 0,
    'runtime.Fiducials.any.seedingMethod': 1,
    'comparam.track.beadtrack.SobelFilterCentering': 1,
    'comparam.track.beadtrack.KernelSigmaForSobel': 1.0,
    'comparam.track.beadtrack.RoundsOfTracking': 4,
    'runtime.BeadTracking.any.numberOfRuns': 4,
    'comparam.autofidseed.autofidseed.TargetNumberOfBeads': 25,
    'comparam.autofidseed.autofidseed.AdjustSizes': 0,
    'comparam.autofidseed.autofidseed.TwoSurfaces': 0,
    'comparam.align.tiltalign.SurfacesToAnalyze': 2,
    'comparam.align.tiltalign.LocalAlignments': 1,
    'comparam.align.tiltalign.RobustFitting': 0,
    'runtime.TiltAlignment.any.enableStretching': 1,
    'comparam.tilt.tilt.THICKNESS': 1000,
    'comparam.tilt.tilt.FakeSIRTiterations': 5,
    'runtime.Trimvol.any.reorient': 2,
}

export {
    defaults
}