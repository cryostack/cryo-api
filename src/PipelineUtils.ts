import bunyan from 'bunyan';
import crypto from 'crypto';
import fs from 'fs';
import glob from 'glob';
import * as t from "io-ts";

import * as etomo from './configDefaults/batchruntomo.configDefaults'
import * as Ietomo from './interfaces/batchruntomo.interfaces'
import { IConfig, ITiltScheme } from './interfaces/pipeline.interfaces';

const kDefaults = {
    idAlgorithm: 'sha256',
    sizeOfIdSlice: 20
}

class PipelineUtils {
    private config: t.TypeOf<typeof IConfig>
    private log: bunyan
    private script: string[]
    private prefix: string

    constructor(config: t.TypeOf<typeof IConfig>) {
        this.config = config
        this.prefix = ''
        this.script = [
            '# This script has been generated automatically by cryoStack',
            '# It is CC0-v1.0, which means you can do whatever you want with it.',
            '# ... but... you probably don\'t want to mess around with it raw, like that.',
            '# instead, if I were you, I would use the cryoStack program to build one just for you.',
            '# Reach out for documentation at cryostack.org',
            '' 
        ]
        this.log = bunyan.createLogger({
            name: 'PipelineUtils'
        })
    }

    public resetScript() {
        this.script = [
            '# This script has been generated automatically by cryoStack',
            '# It is CC0-v1.0, which means you can do whatever you want with it.',
            '# ... but... you probably don\'t want to mess around with it raw, like that.',
            '# instead, if I were you, I would use the cryoStack program to build one just for you.',
            '# Reach out for documentation at cryostack.org',
            '' 
        ]
        return this
    }

    public setPrefix(prefix: string) {
        this.prefix = prefix
    }

    public async makeId() {
        const files = glob.sync(`${this.config.path}/${this.prefix}*.mrc`)
        this.log.info(`Generating dataset ID for the '${this.prefix}' dataset`)
        const processHash = (file: string) => {
            return new Promise((resolve, reject) => {
                const shasum = crypto.createHash(kDefaults.idAlgorithm)
                const input = fs.createReadStream(file)
                input.on('data', (data) => {
                    shasum.update(data)
                })
                input.on('end', () => {
                    const hash = shasum.digest('hex')
                    // this.log.info(`hash: ${hash}`)
                    resolve(hash)
                })
                input.on('error', (error) => {
                    reject(error)
                })
            })
        }
        let hashStack = ''
        for (const file of files) {
            this.log.info(`Calculating the sha256 hash of ${file}`)
            hashStack += await processHash(file)
        }
        this.log.info(`Calculating the sha256 of the whole dataset`)
        const shasum = crypto.createHash(kDefaults.idAlgorithm)
        shasum.update(hashStack)
        return shasum.digest('hex').slice(0, kDefaults.sizeOfIdSlice)
    }

    public buildBatchruntomoDirectiveFile(): string {
        const defaults = etomo.defaults
        const directiveFile = [
            '# This file was created automatically by cryoStack',
            '# As with anything in this package, you can do whatever you want with it.',
            '# But... it will make your life easier in the future if you change the config file fed to the cryoStack pipeline.',
            '# Check it out the documentation at cryostack.org',
            '# Cheers',
            ''
        ]
        const updatedDefault = Object.assign(defaults, this.config.eTomoDirectives)
        for (const directive in updatedDefault) {
            if (updatedDefault.hasOwnProperty(directive)) {
                const key = directive as t.TypeOf<typeof Ietomo.Directives>
                directiveFile.push(`${key} = ${updatedDefault[key]}`)
            }
        }
        return directiveFile.join('\n')
    }

    public echoThis(something: string): PipelineUtils {
        this.script.push(`echo '${something}'`)
        return this
    }

    public echoCurrentPrefix(): PipelineUtils {
        this.script.push(`echo ${this.prefix}`)
        return this
    }

    public alignFrames(args: string = ''): PipelineUtils {
        const nextPrefix = `af.${this.prefix}`
        this.script.push(`for i in \`ls ${this.prefix}*.mrc\``)
        this.script.push(`  do alignframes -in \${i} -o af.\${i} ${args}`)
        this.script.push('done')
        this.setPrefix(nextPrefix)
        return this
    }

    public stackFrames(tiltScheme: t.TypeOf<typeof ITiltScheme>): PipelineUtils {
        switch (tiltScheme.mode) {
            case 'directional': {
                this.log.info('Tilt scheme selected: Directional')
                this.script.push(`newstack \`ls ${this.prefix}*.mrc`)
                break
            }
            case 'bi-directional': {
                const start = tiltScheme.start
                const range = tiltScheme.range
                const increment = tiltScheme.increment
                const expectedNumberOfTilts = (Math.abs(range[0]) + Math.abs(range[0]))/increment + 1
                this.log.info('Tilt scheme selected: Bi-directional')
                this.script.push(`# sanity check`)
                this.script.push(`n=\`ls ${this.prefix}*.mrc | wc -l\``)
                this.script.push(`if [ $n -ne ${expectedNumberOfTilts} ]`)
                this.script.push(`  then echo "We can't process this tilt series, it seems that it is incomplete. Please execute the reconstruction manually or check your files."; exit 1`)
                this.script.push(`fi`)
                const first = Math.abs(range[0] - start) / increment
                const second = expectedNumberOfTilts - first
                this.script.push(`newstack \`ls -r ${this.prefix}*.mrc | head -n ${Math.floor(first)}\` first.mrc`)
                this.script.push(`newstack \`ls ${this.prefix}*.mrc | head -n ${Math.floor(second)}\` second.mrc`)
                this.setPrefix('stacked.' + this.prefix)
                this.script.push(`newstack first.mrc second.mrc ${this.prefix}.mrc`)
                this.script.push(`cp ${this.prefix}.mrc ts.${this.prefix}.mrc`)
                break
            }
        }
        return this
    }

    public batchruntomo(directiveFilename: string): PipelineUtils {
        this.script.push(`batchruntomo -DirectiveFile ${directiveFilename} -RootName ${this.prefix} -CurrentLocation . -StartingStep 0 -EndingStep 20 -cpus ${this.config.cpus}`)
        return this
    }

    public write() {
        this.script.push(
            '\n# End of script. No rights reserved what so ever.'
        )
        return this.script.join('\n')
    }

}

export { 
    PipelineUtils
}