import argparse from 'argparse'
import pm2 from 'pm2'

import shell from 'shelljs'
import { start } from 'repl'
import chalk from 'chalk'


const parser = new argparse.ArgumentParser({
    addHelp: true,
    prog: 'cryostack',
	description: 'Simple CryoStack manager.'
})

const sp = parser.addSubparsers({dest: 'option'})

sp.addParser('up', {
    help: 'Start CryoStack'
})

sp.addParser('down', {
    help: 'Stop CryoStack'
})

const args = parser.parseArgs()

const argsJson = JSON.parse(JSON.stringify(args))
console.log(argsJson)

if ( argsJson.option === 'up' ) {
    const startRedis = shell.exec('sh ./startRedis.sh')
    const startPanel = shell.exec('docker run -d --rm --network host -p 5000:5000 --name cryostack-cp cryostack/control-panel:0.4')

    pm2.connect(function(err) {
        if (err) {
            console.error(err);
            process.exit(2);
        }
        
        pm2.start({
            name: 'cryostack',
            script    : './build/server.js',         // Script to be run
            exec_mode : 'cluster',        // Allows your app to be clustered
            instances : 4,                // Optional: Scales your app by 4
            max_memory_restart : '100M'   // Optional: Restarts your app if it reaches 100Mo
        }, function(err, apps) {
            pm2.disconnect();   // Disconnects from PM2
            console.log(chalk.cyan('CryoStack is up. Enjoy it at: http://localhost:5000'))
            if (err) throw err
        });
    })
} 

if ( argsJson.option === 'down' ) {
    const stopPanel = shell.exec('docker stop cryostack-cp')

    pm2.connect(function(err) {
        if (err) {
            console.error(err);
            process.exit(2);
        }
        
        pm2.delete('cryostack',
        function(err, apps) {
            pm2.disconnect();   // Disconnects from PM2
            const stopRedis = shell.exec('sh ./stopRedis.sh')
            if (err) throw err
        });
    })
} 