const imageAliases = [
    { 
        alias: 'imod',
        image: 'cryostack/imod:4.9.12',
    },
    {
        alias: 'controlPanel',
        image: 'cryostack/control-panel:0.1',
    }
]

export {
    imageAliases
}